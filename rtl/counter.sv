

module Counter (counter_intf intf);

logic [3:0] value;

always_ff @(posedge intf.clk) begin 
    if(intf.rst)
        value<=0;

    if (intf.start) begin
        value<=intf.in;
    end else if(intf.dec && !intf.stop) begin
        value<=value-1'b1;
    end
end 

assign intf.stop = (value==4'b0);
endmodule
