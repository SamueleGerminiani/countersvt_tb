interface counter_intf(input clk,rst);
  
  //declaring the signals
  logic start;
  logic [3:0] in;
  logic dec;
  logic stop;
  
  modport dut (input clk, rst, start, in, dec, output stop);
endinterface
