 #!/bin/sh
rm -rf work
vlib work
vlog +incdir+tb/ -sv tb/testbench.sv rtl/counter.sv -cuname assertions -mfcu assertions/*.sv
vsim -voptargs=+acc -c work.tbench_top -do "run -all; exit" 
gtkwave counter.vcd scounter.gtkw 
