`define COUNTER tbench_top.DUT

checker Ch1(clk,start,in,dec,stop,value);

    assert property (ch1_clocking.p0);

    clocking ch1_clocking @(posedge clk);
            property p0;
                (start && in>0)|->nexttime[1](value>0);
            endproperty
    endclocking

    int p0ATCT=0;
    cover property (ch1_clocking.p0) begin
        p0ATCT++;
    end

    final
        $display("p0ATCT: %d", p0ATCT);


endchecker: Ch1

bind `COUNTER Ch1 ch1_instance(`COUNTER.intf.clk,`COUNTER.intf.start,`COUNTER.intf.in,`COUNTER.intf.dec,`COUNTER.intf.stop,`COUNTER.value);
